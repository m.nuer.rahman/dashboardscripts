#
# OS: OSX High Sierra
# CPU: Intel(R) i7 @ 3.6GHz
# GPU: Radeon Pro 560
# Compiler: Apple LLVM version 9.1 (clang-902.0.39.2)
# Maintainer: robert.maynard@kitware.com
#
cmake_minimum_required(VERSION 2.8)

set(maintainer_email_account "robert.maynard")
set(maintainer_email_domain "kitware.org")

set(CTEST_SITE "dragnipur.kitware")
set(CTEST_BUILD_NAME "OSXHighSierra-AppleClang-9.1")
set(CTEST_CONFIGURATION_TYPE Debug)
set(CTEST_CMAKE_GENERATOR "Unix Makefiles")
set(CTEST_DASHBOARD_ROOT "$ENV{HOME}/Dashboards/MyTests")
set(dashboard_source_name vtk_clang_src)
set(dashboard_binary_name vtkm_clang)
set(dashboard_model Nightly)

set(CTEST_TEST_TIMEOUT 60)

# Common C/C++ flags:

set(dashboard_cache "
CMAKE_BUILD_TYPE:STRING=Debug
BUILD_SHARED_LIBS:BOOL=ON
VTK_DEBUG_LEAKS:BOOL=ON
BUILD_EXAMPLES:BOOL=OFF
VTK_Group_Rendering:BOOL=ON
VTK_Group_StandAlone:BOOL=ON
VTK_WRAP_PYTHON:BOOL=ON
")

include(${CTEST_SCRIPT_DIRECTORY}/vtk/vtk_common.cmake)

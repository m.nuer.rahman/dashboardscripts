#
# OS: Ubuntu 14.04 x86_64
# CPU: Intel(R) Xeon(R) CPU W3670  @ 3.20GHz
# GPU: NVidia GeForce 950
# Compiler: GCC version 5.4
# Maintainer: robert.maynard@kitware.com
#
cmake_minimum_required(VERSION 2.8)

set(ENV{CC} "/usr/bin/gcc-5")
set(ENV{CXX} "/usr/bin/g++-5")

set(ENV{CUDACXX} "/usr/local/cuda/bin/nvcc")

set(maintainer_email_account "robert.maynard")
set(maintainer_email_domain "kitware.org")

set(CTEST_SITE "dejagore.kitware")
set(CTEST_BUILD_NAME "Ubuntu-Sublime-Ninja-GCC-5.4")
set(CTEST_BUILD_CONFIGURATION Debug)
set(CTEST_CMAKE_GENERATOR "Sublime Text 2 - Ninja")

set(build_dir "CMake-ninja-sublime-gcc")
set(source_dir "${build_dir}-src")
set(CTEST_DASHBOARD_ROOT "$ENV{HOME}/Dashboards/MyTests")
set(CTEST_SOURCE_DIRECTORY "${CTEST_DASHBOARD_ROOT}/${source_dir}")
set(CTEST_BINARY_DIRECTORY "${CTEST_DASHBOARD_ROOT}/${build_dir}")

set(CTEST_GIT_COMMAND /usr/bin/git)

# Write initial cache.
set(dashboard_cache "
CMAKE_MAKE_PROGRAM:PATH=/usr/local/bin/ninja
CMAKE_CUDA_COMPILER:FILEPATH=/usr/local/cuda/bin/nvcc
CMAKE_BUILD_TYPE:STRING=Debug
BUILD_QtDialog:BOOL=ON
CMake_TEST_CMakeOnly.AllFindModules_NO_VERSION:STRING=FREETYPE
CMake_TEST_CUDA:STRING=NVIDIA
CMake_TEST_FindOpenGL:BOOL=ON
")

include(${CTEST_SCRIPT_DIRECTORY}/cmake/cmake_common.cmake)

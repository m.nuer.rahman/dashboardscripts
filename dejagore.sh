#!/bin/bash
export DISPLAY=:0.0 # just DISPLAY=:0.0 without export is not enough
export PATH=/usr/local/bin:$PATH #put ninja on the path.

/usr/local/bin/ctest -S ${HOME}/Dashboards/DashboardScripts/dejagore_cmake_subl_gcc54.cmake

#Use a CMake nightly build that has Ninja Multi-Config support
/home/kitware/Dashboards/Support/bin/ctest -S ${HOME}/Dashboards/DashboardScripts/dejagore_cmake_subl_clang38.cmake

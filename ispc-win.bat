
set PATH=C:\Dbd\support\ninja;D:\Dbd\support\cmake-3.18.1\bin;%PATH%

setlocal
call "C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvarsall.bat" x64
call ctest -S "C:\Dbd\DashboardScripts\ispc-win_cmake_make_vs2019_x64.cmake" -VV
call ctest -S "C:\Dbd\DashboardScripts\ispc-win_cmake_ninja-multi_vs2019_x64.cmake" -VV
endlocal

setlocal
call "C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvarsall.bat" x86
call ctest -S "C:\Dbd\DashboardScripts\ispc-win_cmake_ninja_vs2019_x32.cmake" -VV
endlocal

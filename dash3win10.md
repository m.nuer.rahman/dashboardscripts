The BullseyeCoverage installer's VS 2015 modifies the `*.bat` and `*/*.bat`
files in `C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\bin`:

* `vcvars32.bat`
* `vcvarsphonex86.bat`
* `amd64_arm/vcvarsamd64_arm.bat`
* `amd64/vcvars64.bat`
* `amd64_x86/vcvarsamd64_x86.bat`
* `x86_amd64/vcvarsx86_amd64.bat`
* `x86_arm/vcvarsphonex86_arm.bat`
* `x86_arm/vcvarsx86_arm.bat`

It adds one line to the bottom of each file:

    @set PATH=C:\Program Files (x86)\BullseyeCoverage\bin;%PATH%

This may be how it injects cl.exe and link.exe into VS IDE builds.

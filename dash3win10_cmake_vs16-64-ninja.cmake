set(CTEST_SITE "dash3win10.kitware")
set(CTEST_BUILD_NAME "vs16-64-ninja")
set(CTEST_BUILD_CONFIGURATION Release)
set(CTEST_CMAKE_GENERATOR "Ninja")
set(CTEST_BUILD_FLAGS "-k0")
set(CTEST_TEST_TIMEOUT 300)
set(CTEST_TEST_ARGS PARALLEL_LEVEL 6)

set(ENV{CMAKE_PREFIX_PATH} "c:/Qt/5.13.0/msvc2017_64")

set(dashboard_source_name CMake-${CTEST_BUILD_NAME}-src)
set(dashboard_binary_name CMake-${CTEST_BUILD_NAME}-bld)
set(dashboard_cache "
BUILD_QtDialog:BOOL=ON
CMake_TEST_FindOpenGL:BOOL=ON
CMake_TEST_IPO_WORKS_C:BOOL=ON
CMake_TEST_IPO_WORKS_CXX:BOOL=ON
")

include(${CTEST_SCRIPT_DIRECTORY}/cmake/cmake_common.cmake)

cd C:\Dashboards\DashboardScripts

setlocal
set VSCMD_START_DIR=.
call "C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvarsall.bat" x64
set PATH=C:\Dashboards\bin;%PATH%
"C:\Program Files\CMake\bin\ctest.exe" -C Release -S dash3win10_cmake_vs16-64-ninja.cmake -V > C:\Dashboards\Logs\cmake_vs16-64-ninja.txt 2>&1
endlocal

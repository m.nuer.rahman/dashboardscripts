#!/bin/bash

# This retrievs the directory of the currently running script in a way that
# should work across GNU and Non-GNU environments alike (Linux, Apple, AIX,
# etc.)
SCRIPT_DIR=$(perl -MCwd -e 'print Cwd::abs_path shift' $(dirname ${BASH_SOURCE}))

# Update ourselves and re-run
cd "${SCRIPT_DIR}"
git log --pretty=format:"%h %aI [%an] %s" | head -1
if [ "${DASHBOARD_SCRIPTS_SKIP_UPDATE}" != "1" ]
then
  cd "${SCRIPT_DIR}"
  if git pull --ff-only
  then
    git submodule update --init --recursive
  fi
  export DASHBOARD_SCRIPTS_SKIP_UPDATE=1
  exec "${BASH_SOURCE}" "$@"
  exit $?
fi

# Source any site-specific variables or scripts
if [ -f ${HOME}/.dashboard ]
then
  source ${HOME}/.dashboard
fi

CTEST=${HOME}/common/cmake/latest/bin/ctest

# Make sure our tmp direcotry is on a RAM disk
export TMPDIR=/dev/shm/${USER}/tmp
mkdir -p ${TMPDIR}

######################################################################
# CMake
######################################################################
if [ "${DASHBOARD_SCRIPTS_SKIP_CMAKE}" != "1" ]
then

LOG_DIR=${HOME}/Dashboards/CMake/Logs
BASE_DIR=${HOME}/Dashboards/CMake
mkdir -p ${LOG_DIR} ${BASE_DIR}
pushd ${BASE_DIR}

# Intel compilers
for INTEL_VERSION in 16.0.{0,1,2} 17.0.{0,1,2,3,4,5,6,7,8} 18.0.{0,1,2,3,5} 19.0.{0,1,2,3,4,5} 19.1.{0,1,2}
do
  ${CTEST} -VV \
    -S ${SCRIPT_DIR}/aaargh_cmake_intel.cmake \
    -DINTEL_VERSION=${INTEL_VERSION} 2>&1 | \
  tee ${LOG_DIR}/aaargh_cmake_intel-${INTEL_VERSION}.log
done

# Coverity scan
${CTEST} -VV -S ${SCRIPT_DIR}/aaargh_cmake_coverity.cmake 2>&1 | \
  tee ${LOG_DIR}/aaargh_cmake_coverity.log

fi

######################################################################
# DIY
######################################################################
if [ "${DASHBOARD_SCRIPTS_SKIP_DIY}" != "1" ]
then

LOG_DIR=${HOME}/Dashboards/diy/Logs
BASE_DIR=${HOME}/Dashboards/diy
mkdir -p ${LOG_DIR} ${BASE_DIR}
pushd ${BASE_DIR}

${CTEST} -VV -S ${SCRIPT_DIR}/aaargh_diy_intel19.cmake 2>&1 | \
  tee ${LOG_DIR}/aaargh_diy_intel19.log
${CTEST} -VV -S ${SCRIPT_DIR}/aaargh_diy_intel19-openmpi.cmake 2>&1 | \
  tee ${LOG_DIR}/aaargh_diy_intel19-openmpi.log

fi

# Client maintainer: chuck.atkins@kitware.com
set(CTEST_SITE "aaargh.kitware.com")
set(CTEST_BUILD_NAME "Linux-EL7-Intel-${INTEL_VERSION}")
set(CTEST_BUILD_CONFIGURATION Release)
set(CTEST_CMAKE_GENERATOR "Ninja")
set(CTEST_BUILD_FLAGS "-k0")
set(CTEST_TEST_ARGS PARALLEL_LEVEL 18)
find_program(CMAKE_MAKE_PROGRAM NAMES ninja PATHS /usr/local/bin)

set(CTEST_DASHBOARD_ROOT "${CMAKE_CURRENT_BINARY_DIR}/Builds/Intel ${INTEL_VERSION}")
set(dashboard_model "Nightly")

find_package(EnvModules REQUIRED)
env_module(purge)
env_module_avail(intel/${INTEL_VERSION} mod_name)
env_module(load ${mod_name})

env_module_list(mods_loaded)
message("Modules loaded: ${mods_loaded}")

set(ENV{CC}  icc)
set(ENV{CXX} icpc)
set(ENV{FC}  ifort)

set(dashboard_cache "
CMAKE_USE_SYSTEM_LIBRARIES:BOOL=ON
CMAKE_USE_SYSTEM_LIBRARY_JSONCPP:BOOL=OFF
CMAKE_USE_SYSTEM_LIBRARY_LIBARCHIVE:BOOL=OFF
CMAKE_USE_SYSTEM_LIBRARY_ZSTD:BOOL=OFF
CMake_TEST_CMakeOnly.LinkInterfaceLoop_TIMEOUT:STRING=300

CMake_TEST_FindEnvModules:BOOL=ON
")

# Intel 16.0.2 and 16.0.3 have a bug that breaks automoc
# See https://software.intel.com/en-us/forums/intel-c-compiler/topic/628638
if ("${INTEL_VERSION}" MATCHES "16.0.(2|3)")
  string(APPEND dashboard_cache "
CMake_TEST_Qt5:BOOL=OFF
")
endif()

include(${CTEST_SCRIPT_DIRECTORY}/cmake/cmake_common.cmake)

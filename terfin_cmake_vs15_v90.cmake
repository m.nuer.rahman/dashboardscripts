set(CTEST_SITE "terfin.kitware")
set(CTEST_BUILD_NAME "vs15-32-ide-v90")
set(CTEST_BUILD_CONFIGURATION Release)
set(CTEST_CMAKE_GENERATOR "Visual Studio 15 2017")
set(CTEST_CMAKE_GENERATOR_TOOLSET "v90")
set(exclude
  CSharpLinkFromCxx # Toolset and framework version mismatch.
  CSharpLinkToCxx # Toolset and framework version mismatch.
  RunCMake.CSharpReferenceImport # Toolset and framework version mismatch.
  )
string(REPLACE ";" "|" exclude "${exclude}")
set(CTEST_TEST_ARGS
  PARALLEL_LEVEL 16
  EXCLUDE "^(${exclude})$"
  )

set(dashboard_source_name CMake-${CTEST_BUILD_NAME}-src)
set(dashboard_binary_name CMake-${CTEST_BUILD_NAME}-bld)

set(dashboard_cache "
CMAKE_Fortran_COMPILER:FILEPATH=
CMake_TEST_EXTERNAL_CMAKE:PATH=C:/Dashboards/My Tests/CMake-vs15-64-ninja-bld/bin
")

#set(dashboard_no_submit 1)
set(dashboard_no_KWSys 1)
include(${CTEST_SCRIPT_DIRECTORY}/cmake/cmake_common.cmake)

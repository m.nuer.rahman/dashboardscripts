set(CTEST_SITE "terfin.kitware")
set(CTEST_BUILD_NAME "vs15-64-ide")
set(CTEST_BUILD_CONFIGURATION Release)
set(CTEST_CMAKE_GENERATOR "Visual Studio 15 2017")
set(CTEST_CMAKE_GENERATOR_PLATFORM "x64")
set(CTEST_TEST_ARGS PARALLEL_LEVEL 16)

set(ENV{CMAKE_PREFIX_PATH} "c:/Qt/5.9.1/msvc2017_64")

set(dashboard_source_name CMake-${CTEST_BUILD_NAME}-src)
set(dashboard_binary_name CMake-${CTEST_BUILD_NAME}-bld)

set(dashboard_cache "
CMAKE_Fortran_COMPILER:FILEPATH=
CMake_TEST_EXTERNAL_CMAKE:PATH=C:/Dashboards/My Tests/CMake-vs15-64-ninja-bld/bin
CMake_TEST_FindODBC:BOOL=ON
CMake_TEST_IPO_WORKS_C:BOOL=ON
CMake_TEST_IPO_WORKS_CXX:BOOL=ON
")

#set(dashboard_no_submit 1)
set(dashboard_no_KWSys 1)
include(${CTEST_SCRIPT_DIRECTORY}/cmake/cmake_common.cmake)

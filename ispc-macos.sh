#!/bin/bash
export DISPLAY=:0.0 # just DISPLAY=:0.0 without export is not enough

export PATH=${HOME}/Dashboards/Support/cmake-3.18.0/CMake.app/Contents/bin:$PATH


ctest -S ${HOME}/Dashboards/DashboardScripts/ispc-macos_cmake_ninja-multi.cmake
ctest -S ${HOME}/Dashboards/DashboardScripts/ispc-macos_cmake_ninja.cmake
ctest -S ${HOME}/Dashboards/DashboardScripts/ispc-macos_cmake_make.cmake

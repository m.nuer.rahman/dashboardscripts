#
# OS: Windows 10
# Compiler: Visual Studio 2019
# Maintainer: robert.maynard@kitware.com
#
set(maintainer_email_account "robert.maynard")
set(maintainer_email_domain "kitware.org")

set(CTEST_SITE "ispc-win.kitware")
set(CTEST_BUILD_NAME "make-ispc-vs2019-64")
set(CTEST_BUILD_CONFIGURATION Debug)
set(CTEST_CMAKE_GENERATOR "Unix Makefile")
set(CTEST_BUILD_FLAGS -j14)
set(CTEST_TEST_ARGS PARALLEL_LEVEL 14)

set(ENV{ISPC} "C:/Dbd/support/ispc-v1.13.0/bin/ispc.exe")

set(dashboard_model Nightly)
set(dashboard_source_name CMakeMake-src)
set(dashboard_binary_name CMakeMake-bld)

set(dashboard_cache "
CMake_TEST_ISPC:BOOL=ON
")

include(${CTEST_SCRIPT_DIRECTORY}/cmake/cmake_common.cmake)

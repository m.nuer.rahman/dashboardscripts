set(CTEST_SITE "terfin.kitware")
set(CTEST_BUILD_NAME "vs15-64-ninja")
set(CTEST_BUILD_CONFIGURATION Release)
set(CTEST_CMAKE_GENERATOR "Ninja")
set(CTEST_BUILD_FLAGS "-k0")
set(CTEST_TEST_ARGS PARALLEL_LEVEL 16)

set(ENV{CMAKE_PREFIX_PATH} "c:/Qt/5.9.1/msvc2017_64")

set(dashboard_source_name CMake-${CTEST_BUILD_NAME}-src)
set(dashboard_binary_name CMake-${CTEST_BUILD_NAME}-bld)
set(dashboard_cache "
BUILD_QtDialog:BOOL=ON
CMake_TEST_FindODBC:BOOL=ON
CMake_TEST_FindOpenGL:BOOL=ON
CMake_TEST_FindOpenMP:BOOL=ON
CMake_TEST_FindOpenMP_C:BOOL=ON
CMake_TEST_FindOpenMP_CXX:BOOL=ON
CMake_TEST_FindOpenMP_Fortran:BOOL=OFF
CMake_TEST_IPO_WORKS_C:BOOL=ON
CMake_TEST_IPO_WORKS_CXX:BOOL=ON
")

#set(dashboard_no_submit 1)
include(${CTEST_SCRIPT_DIRECTORY}/cmake/cmake_common.cmake)

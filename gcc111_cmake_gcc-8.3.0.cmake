# Client maintainer: chuck.atkins@kitware.com
set(CTEST_SITE "gcc111.fsffrance.org")
set(CTEST_BUILD_NAME "AIX-7.1_GCC-8.3.0")
set(CTEST_BUILD_CONFIGURATION Release)
set(CTEST_CMAKE_GENERATOR "Unix Makefiles")
set(CTEST_BUILD_FLAGS "-k -j16")
set(CTEST_DASHBOARD_ROOT "${CMAKE_CURRENT_BINARY_DIR}/Builds/GCC_8.3.0")
set(dashboard_model "Nightly")

set(ENV{CC}  /opt/freeware/bin/gcc)
set(ENV{CXX} /opt/freeware/bin/g++)
set(ENV{FC}  /opt/freeware/bin/gfortran)

set(exclude
  LinkStatic # https://gna.org/bugs/?20944

  # RPM tests that fail without objdump installed
  RunCMake.CPack_RPM.DEBUGINFO
  RunCMake.CPack_RPM.EXTRA_SLASH_IN_PATH
  RunCMake.CPack_RPM.SINGLE_DEBUGINFO
  )
string(REPLACE ";" "|" exclude "${exclude}")
set(CTEST_TEST_ARGS
  PARALLEL_LEVEL 16
  EXCLUDE "^(${exclude})$"
)

set(dashboard_cache "
CURSES_NCURSES_LIBRARY:FILEPATH=IGNORE
CMake_TEST_CMakeOnly.AllFindModules_NO_VERSION:STRING=GETTEXT
CMake_TEST_FILESYSTEM_1S:BOOL=ON
")

include(${CTEST_SCRIPT_DIRECTORY}/cmake/cmake_common.cmake)

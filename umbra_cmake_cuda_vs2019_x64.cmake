#
# OS: Windows 10
# Compiler: Visual Studio 2019
# Maintainer: robert.maynard@kitware.com
#
set(maintainer_email_account "robert.maynard")
set(maintainer_email_domain "kitware.org")

set(CTEST_SITE "umbra.kitware")
set(CTEST_BUILD_NAME "ninja-cuda-vs19-64")
set(CTEST_BUILD_CONFIGURATION Debug)
set(CTEST_CMAKE_GENERATOR "Ninja")
set(CTEST_TEST_ARGS PARALLEL_LEVEL 6)
set(dashboard_source_name CMake-${CTEST_BUILD_NAME}-src)
set(dashboard_binary_name CMake-${CTEST_BUILD_NAME}-build)
set(dashboard_cache "
CMake_TEST_CUDA:STRING=NVIDIA
")
include(${CTEST_SCRIPT_DIRECTORY}/cmake/cmake_common.cmake)

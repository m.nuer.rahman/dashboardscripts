set(CTEST_SITE                  "moonshade.kitware")
set(CTEST_BUILD_NAME            "macOS-ninja-ub")
set(CTEST_BUILD_CONFIGURATION   "Release")
set(CTEST_CMAKE_GENERATOR       "Ninja")
set(CTEST_BUILD_FLAGS           "-k0")
set(CTEST_TEST_ARGS             PARALLEL_LEVEL 8)
set(CTEST_TEST_CTEST 0)

set(dashboard_model Nightly)
set(dashboard_source_name CMakeNinja-src)
set(dashboard_binary_name CMakeNinja-bld)

set(ENV{CMAKE_OSX_ARCHITECTURES} "x86_64;arm64")
set(ENV{CMAKE_PREFIX_PATH} "/Users/kitware/SDKs/qt-5.15.2-macosx10.13-x86_64-arm64")
set(ENV{DEVELOPER_DIR} "/Applications/Xcode-12.3.app/Contents/Developer")

set(dashboard_cache "
CMAKE_Swift_COMPILER:FILEPATH=
BUILD_QtDialog:BOOL=TRUE
")

include(${CTEST_SCRIPT_DIRECTORY}/cmake/cmake_common.cmake)

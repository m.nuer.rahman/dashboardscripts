#!/usr/bin/env bash

export "PATH=${HOME}/Dashboards/bin:$PATH"

/Applications/CMake.app/Contents/bin/ctest -S ${HOME}/Dashboards/DashboardScripts/moonshade_cmake_cont.cmake -V > ${HOME}/Dashboards/Logs/cmake_cont.log 2>&1

#!/usr/bin/env bash

export "PATH=${HOME}/Dashboards/bin:$PATH"

"/usr/bin/ctest" -S "${HOME}/Dashboards/DashboardScripts/minoc_cmake_cont.cmake" -V >"$HOME/Dashboards/Logs/cmake_cont.log" 2>&1

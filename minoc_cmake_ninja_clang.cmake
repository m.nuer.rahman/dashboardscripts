set(CTEST_SITE "minoc.kitware")
set(CTEST_BUILD_NAME "linux-ninja-clang")
set(CTEST_BUILD_CONFIGURATION Release)
set(CTEST_CMAKE_GENERATOR "Ninja")
set(CTEST_TEST_ARGS
  PARALLEL_LEVEL 8
  )

set(dashboard_model Nightly)
set(dashboard_source_name CMakeNinjaClang-src)
set(dashboard_binary_name CMakeNinjaClang-bld)

set(ENV{CC} "clang-11")
set(ENV{CXX} "clang++-11")
set(ENV{CUDACXX} "clang++-11")

set(dashboard_cache "
CMake_TEST_CUDA:STRING=Clang
CMAKE_SKIP_BOOTSTRAP_TEST:BOOL=TRUE
")

include(${CTEST_SCRIPT_DIRECTORY}/cmake/cmake_common.cmake)

# Client maintainer: chuck.atkins@kitware.com
set(CTEST_SITE "aaargh.kitware.com")
set(CTEST_BUILD_CONFIGURATION Release)
set(CTEST_CMAKE_GENERATOR "Ninja")
set(CTEST_BUILD_FLAGS "-k 0")
set(TEST_TEST_ARGS PARALLEL_LEVEL 4)
find_program(CMAKE_MAKE_PROGRAM NAMES ninja PATHS /usr/local/bin)

set(CTEST_BUILD_NAME "el7-x86_64-intel19")
set(dashboard_model Nightly)
set(CTEST_DASHBOARD_ROOT "${CMAKE_CURRENT_BINARY_DIR}/Builds/${CTEST_BUILD_NAME}")

find_package(EnvModules REQUIRED)
env_module(purge)
env_module(load intel/19.0.3.199)

set(ENV{CC}  icc)
set(ENV{CXX} icpc)
set(ENV{FC}  ifort)

set(dashboard_cache "
mpi:bool=OFF
")

include(${CMAKE_CURRENT_LIST_DIR}/diy/diy_common.cmake)

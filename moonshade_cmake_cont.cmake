set(CTEST_SITE "moonshade.kitware")
set(CTEST_BUILD_NAME "continuous-macOS-ninja")
set(CTEST_BUILD_CONFIGURATION Release)
set(CTEST_CMAKE_GENERATOR "Ninja")
set(CTEST_BUILD_FLAGS "-k0")

set(CTEST_TEST_ARGS PARALLEL_LEVEL 8)
set(CTEST_TEST_TIMEOUT 800)

# Do not let this test take too long during the continuous.
set(ENV{KWSYS_TEST_PROCESS_1_COUNT} 11)

set(ENV{CMAKE_PREFIX_PATH} "/Users/kitware/SDKs/qt-5.8.0-clang_64")
set(ENV{DEVELOPER_DIR} "/Applications/Xcode-12.3.app/Contents/Developer")

set(dashboard_source_name "CMakeCont-src")
set(dashboard_binary_name "CMakeCont-bld")
set(dashboard_model "Continuous")

set(dashboard_cache "
CMAKE_Swift_COMPILER:FILEPATH=/usr/bin/swiftc
CMake_TEST_FindOpenMP:BOOL=ON
CMake_TEST_FindOpenMP_C:BOOL=ON
CMake_TEST_FindOpenMP_CXX:BOOL=ON
CMake_TEST_NO_FindPackageModeMakefileTest:BOOL=ON
BUILD_QtDialog:BOOL=TRUE
")

include(${CTEST_SCRIPT_DIRECTORY}/cmake/cmake_common.cmake)

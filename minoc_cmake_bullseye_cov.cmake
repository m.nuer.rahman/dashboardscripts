cmake_minimum_required(VERSION 3.3)

set(CTEST_SITE        "minoc.kitware")
set(CTEST_BUILD_NAME  "Linux-bullseye-cov")
set(CTEST_CMAKE_GENERATOR "Unix Makefiles")
set(CTEST_BUILD_CONFIGURATION Debug)
set(CTEST_BUILD_FLAGS "-j8")
set(CTEST_TEST_ARGS
  PARALLEL_LEVEL 8
  EXCLUDE "^(CxxDialect)"
  )

set(bullseye_root "/opt/BullseyeCoverage/current")

# Place bullseye last in PATH so that its tools are available but its compilers are not first.
set(ENV{PATH} "$ENV{PATH}:${bullseye_root}/bin")

macro(cov01 v)
  execute_process(COMMAND "${bullseye_root}/bin/cov01" -${v} RESULT_VARIABLE result)
  if(result)
    message(FATAL_ERROR "could not run cov01 -${v}: ${result}")
  endif()
endmacro(cov01)

macro(dashboard_hook_start)
  # Put coverage file in build tree now that we know where it is.
  set(ENV{COVFILE} "${CTEST_BINARY_DIRECTORY}/CMake.cov")

  # turn off coverage for configure step
  cov01(0)
endmacro()

macro(dashboard_hook_build)
  # turn on coverage for build and test
  cov01(1)
endmacro()

# Shrink stress tests when running with bullseye.
set(ENV{KWSYS_TEST_PROCESS_1_COUNT} 11)

set(dashboard_source_name CMakeBullseyeCov)
set(dashboard_binary_name CMakeBullseyeCov-build)

set(dashboard_do_coverage 1)
set(dashboard_cache "
CTEST_NO_TEST_HOME:BOOL=ON
CMake_TEST_Qt4:BOOL=OFF
CMake_TEST_Qt5:BOOL=OFF
CMAKE_C_COMPILER:PATH=${bullseye_root}/bin/gcc-8
CMAKE_CXX_COMPILER:PATH=${bullseye_root}/bin/g++-8
")

include(${CTEST_SCRIPT_DIRECTORY}/cmake/cmake_common.cmake)

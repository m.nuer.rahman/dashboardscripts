#!/bin/bash

# This retrievs the directory of the currently running script in a way that
# should work across GNU and Non-GNU environments alike (Linux, Apple, AIX,
# etc.)
SCRIPT_DIR=$(perl -MCwd -e 'print Cwd::abs_path shift' $(dirname ${BASH_SOURCE}))

# Update ourselves and re-run
pushd "${SCRIPT_DIR}"
git log --pretty=format:"%h %aI [%an] %s" | head -1
if [ "${DASHBOARD_SCRIPTS_SKIP_UPDATE}" != "1" ]
then
  if git pull --ff-only
  then
    git submodule update --init --recursive
  fi
  popd
  export DASHBOARD_SCRIPTS_SKIP_UPDATE=1
  exec "${BASH_SOURCE}" "$@"
  exit $?
fi
popd

# Source any site-specific variables or scripts
if [ -f ${HOME}/.dashboard ]
then
  source ${HOME}/.dashboard
fi

CTEST=${HOME}/common/cmake/latest/bin/ctest

# Make sure our tmp directory is on a RAM disk
export TMPDIR=/dev/shm/${USER}/tmp
mkdir -p ${TMPDIR}

######################################################################
# CMake
######################################################################
if [ "${DASHBOARD_SCRIPTS_SKIP_CMAKE}" != "1" ]
then

LOG_DIR=${HOME}/Dashboards/CMake/Logs
BASE_DIR=${HOME}/Dashboards/CMake
mkdir -p ${LOG_DIR} ${BASE_DIR}
pushd ${BASE_DIR}

${CTEST} -VV -S ${SCRIPT_DIR}/gcc135_cmake_gcc-4.8.cmake 2>&1   | \
  tee ${LOG_DIR}/gcc135_cmake_gcc-4.8.log
${CTEST} -VV -S ${SCRIPT_DIR}/gcc135_cmake_xl-16.1.1.cmake 2>&1 | \
  tee ${LOG_DIR}/gcc135_cmake_xl-16.1.1.log
${CTEST} -VV -S ${SCRIPT_DIR}/gcc135_cmake_pgi-19.10.cmake 2>&1 | \
  tee ${LOG_DIR}/gcc135_cmake_pgi-19.10.log

fi

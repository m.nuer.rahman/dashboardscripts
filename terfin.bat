cd C:\Dashboards\DashboardScripts

setlocal
call "terfin-ninja.bat" >C:\Dashboards\Logs\terfin-ninja.log 2>&1
endlocal

setlocal
set VSCMD_START_DIR=.
call "C:\Program Files (x86)\Microsoft Visual Studio\2017\Professional\VC\Auxiliary\Build\vcvarsall.bat" x64
set PATH=C:\Dashboards\bin;%PATH%
set PATH=C:\Dashboards\ninja-git;%PATH%
"C:\Program Files\CMake\bin\ctest.exe" -C Release -S terfin_cmake_vs15-64-ninja.cmake -V >C:\Dashboards\Logs\terfin_cmake_vs15-64-ninja.log 2>&1
endlocal

"c:\Program Files\CMake\bin\ctest.exe" -C Release -S terfin_cmake_vs9.cmake -V >C:\Dashboards\Logs\terfin_cmake_vs9.log 2>&1
"c:\Program Files\CMake\bin\ctest.exe" -C Release -S terfin_cmake_vs15_x64.cmake -V >C:\Dashboards\Logs\terfin_cmake_vs15_x64.log 2>&1
"c:\Program Files\CMake\bin\ctest.exe" -C Release -S terfin_cmake_vs15_v90.cmake -V >C:\Dashboards\Logs\terfin_cmake_vs15_v90.log 2>&1
"c:\Program Files\CMake\bin\ctest.exe" -C Release -S terfin_cmake_vs16_x64.cmake -V >C:\Dashboards\Logs\terfin_cmake_vs16_x64.log 2>&1

setlocal
set VSCMD_START_DIR=.
call "C:\Program Files (x86)\Microsoft Visual Studio\2017\Professional\VC\Auxiliary\Build\vcvarsall.bat" x64
"c:\Program Files\CMake\bin\ctest.exe" -S terfin_cmake_nmake.cmake -V >C:\Dashboards\Logs\terfin_cmake_nmake.log 2>&1
endlocal

setlocal
set VSCMD_START_DIR=.
call "C:\Program Files (x86)\Microsoft Visual Studio\2017\Professional\VC\Auxiliary\Build\vcvarsall.bat" x86
"C:\Program Files\CMake\bin\ctest.exe" -C Release -S terfin_cmake_jom.cmake -V >C:\Dashboards\Logs\terfin_cmake_jom.log 2>&1
endlocal

terfin-cont.bat

cd C:\Dashboards\DashboardScripts

rem If after 8pm, sleep until midnight.
"c:\Program Files\Git\bin\bash.exe" -c "while test $(date +%%H) -ge 20; do sleep 60; done"
rem Wait until morning.
"c:\Program Files\Git\bin\bash.exe" -c "while test $(date +%%H) -lt 7; do sleep 60; done"

setlocal
set PATH=C:\Dashboards\bin;%PATH%
"c:\Program Files\CMake\bin\ctest.exe" -C Release -S terfin_cmake_cont_vs16_x64.cmake -V >C:\Dashboards\Logs\terfin_cmake_cont_vs16_x64.log 2>&1
endlocal

set(CTEST_SITE                  "moonshade.kitware")
set(CTEST_BUILD_NAME            "macOS-xcode-ub")
set(CTEST_BUILD_CONFIGURATION   "Release")
set(CTEST_CMAKE_GENERATOR       "Xcode")
set(CTEST_CMAKE_GENERATOR_TOOLSET com.apple.compilers.llvm.clang.1_0)
set(CTEST_TEST_TIMEOUT 500)
set(CTEST_TEST_CTEST 0)

set(exclude
  )
string(REPLACE ";" "|" exclude "${exclude}")
set(CTEST_TEST_ARGS
  EXCLUDE "^(${exclude})$"
  PARALLEL_LEVEL 8
  )

set(dashboard_model Nightly)
set(dashboard_source_name CMakeXcodeUB-src)
set(dashboard_binary_name CMakeXcodeUB-bld)

set(ENV{CMAKE_OSX_ARCHITECTURES} "x86_64;arm64")
set(ENV{CMAKE_PREFIX_PATH} "/Users/kitware/SDKs/qt-5.15.2-macosx10.13-x86_64-arm64")
set(ENV{DEVELOPER_DIR} "/Applications/Xcode-12.3.app/Contents/Developer")

set(dashboard_cache "
CMake_TEST_EXTERNAL_CMAKE:PATH=/Users/kitware/Dashboards/My Tests/CMakeNinja-bld/bin
")

include(${CTEST_SCRIPT_DIRECTORY}/cmake/cmake_common.cmake)

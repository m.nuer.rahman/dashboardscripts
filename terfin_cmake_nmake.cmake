set(CTEST_SITE "terfin.kitware")
set(CTEST_BUILD_NAME "vs15-64-nmake")
set(CTEST_BUILD_CONFIGURATION Release)
set(CTEST_CMAKE_GENERATOR "NMake Makefiles")
set(CTEST_TEST_ARGS PARALLEL_LEVEL 16)

set(dashboard_source_name CMake-${CTEST_BUILD_NAME}-src)
set(dashboard_binary_name CMake-${CTEST_BUILD_NAME}-bld)

set(dashboard_cache "
CMake_TEST_EXTERNAL_CMAKE:PATH=C:/Dashboards/My Tests/CMake-vs15-64-ninja-bld/bin
CMake_TEST_IPO_WORKS_C:BOOL=ON
CMake_TEST_IPO_WORKS_CXX:BOOL=ON
")

set(dashboard_no_KWSys 1)
include(${CTEST_SCRIPT_DIRECTORY}/cmake/cmake_common.cmake)

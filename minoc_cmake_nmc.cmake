set(CTEST_SITE "minoc.kitware")
set(CTEST_BUILD_NAME "linux-ninja-multi")
set(CTEST_BUILD_CONFIGURATION Release)
set(CTEST_CMAKE_GENERATOR "Ninja Multi-Config")
set(CTEST_BUILD_FLAGS -j8)
set(CTEST_TEST_ARGS
  PARALLEL_LEVEL 8
  )

set(dashboard_model Nightly)
set(dashboard_source_name CMakeNMC-src)
set(dashboard_binary_name CMakeNMC-bld)

set(dashboard_cache "
")

include(${CTEST_SCRIPT_DIRECTORY}/cmake/cmake_common.cmake)

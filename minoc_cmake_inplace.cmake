set(CTEST_SITE "minoc.kitware")
set(CTEST_BUILD_NAME "Linux-InPlace")
set(CTEST_BUILD_CONFIGURATION Release)
set(CTEST_CMAKE_GENERATOR "Unix Makefiles")
set(dashboard_bootstrap 8)
set(CTEST_BUILD_FLAGS -j8)
set(CTEST_SOURCE_NAME CMakeInPlace)
set(CTEST_BINARY_NAME CMakeInPlace)
set(CTEST_DASHBOARD_ROOT "$ENV{HOME}/Dashboards/My Tests")
set(CTEST_SOURCE_DIRECTORY "${CTEST_DASHBOARD_ROOT}/${CTEST_SOURCE_NAME}")
set(CTEST_BINARY_DIRECTORY "${CTEST_DASHBOARD_ROOT}/${CTEST_BINARY_NAME}")

# remove the source tree which is the binary tree
file(REMOVE_RECURSE "${CTEST_DASHBOARD_ROOT}/CMakeInPlace")

# Get a new copy of the repo.  The regular script will configure the proper
# branch and checkout the content.
execute_process(
  COMMAND git clone -n -- https://gitlab.kitware.com/cmake/cmake.git
          ${CTEST_SOURCE_DIRECTORY})

set(dashboard_cache "
CMake_TEST_FindGTK2:BOOL=ON
")

# now run the regular script
include(${CTEST_SCRIPT_DIRECTORY}/cmake/cmake_common.cmake)

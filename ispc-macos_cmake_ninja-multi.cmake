set(CTEST_SITE "ispc-macos.kitware")
set(CTEST_BUILD_NAME "macos-ninja-multi-config")
set(CTEST_BUILD_CONFIGURATION Debug)
set(CTEST_CMAKE_GENERATOR "Ninja Multi-Config")
set(CTEST_BUILD_FLAGS -j14)
set(CTEST_TEST_ARGS
  PARALLEL_LEVEL 14
  )

set(ENV{ISPC} "/Users/svc-dashboard/Dashboards/Support/ispc-v1.13.0/bin/ispc")

set(dashboard_model Nightly)
set(dashboard_source_name CMakeNMC-src)
set(dashboard_binary_name CMakeNMC-bld)

set(dashboard_cache "
CMake_TEST_ISPC:BOOL=ON
")

include(${CTEST_SCRIPT_DIRECTORY}/cmake/cmake_common.cmake)

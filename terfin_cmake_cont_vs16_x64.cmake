set(CTEST_SITE "terfin.kitware")
set(CTEST_BUILD_NAME "continuous-vs16-64-ide")
set(CTEST_BUILD_CONFIGURATION Release)
set(CTEST_CMAKE_GENERATOR "Visual Studio 16 2019")
set(CTEST_TEST_ARGS PARALLEL_LEVEL 16)
set(CTEST_BUILD_FLAGS "-m")

set(ENV{CMAKE_PREFIX_PATH} "c:/Qt/5.9.1/msvc2017_64")

set(dashboard_binary_name CMakeCont-bld)
set(dashboard_source_name CMakeCont-src)
set(dashboard_model Continuous)

set(dashboard_cache "
CMAKE_Fortran_COMPILER:FILEPATH=
CMake_MSVC_PARALLEL:BOOL=OFF
CMake_TEST_IPO_WORKS_C:BOOL=ON
CMake_TEST_IPO_WORKS_CXX:BOOL=ON
")

# Build in parallel but not for tests.
set(ENV{CFLAGS} "-MP16")
set(ENV{CXXFLAGS} "-MP16")
macro(dashboard_hook_build)
  set(ENV{CFLAGS} "")
  set(ENV{CXXFLAGS} "")
endmacro()

set(dashboard_no_KWSys 1)
include(${CTEST_SCRIPT_DIRECTORY}/cmake/cmake_common.cmake)

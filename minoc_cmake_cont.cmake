set(CTEST_SITE "minoc.kitware")
set(CTEST_BUILD_NAME "continuous-linux-make")
set(CTEST_CMAKE_GENERATOR "Unix Makefiles")
set(CTEST_BUILD_CONFIGURATION Release)
set(CTEST_BUILD_FLAGS "-j8")

# Exclude some tests.
set(exclude
  "Qt5Autogen.RerunMocPlugin" # pending further investigation on minoc
  )
string(REPLACE ";" "|" exclude "${exclude}")

set(CTEST_TEST_ARGS
  PARALLEL_LEVEL 8
  EXCLUDE "^(${exclude})$"
  )
set(CTEST_TEST_TIMEOUT 800)

# Do not let this test take too long during the continuous.
set(ENV{KWSYS_TEST_PROCESS_1_COUNT} 11)

set(ENV{SWIFTC} "/opt/swift/5.0.1-ubuntu18.04/bin/swiftc")

set(dashboard_source_name "CMakeCont-src")
set(dashboard_binary_name "CMakeCont-bld")
set(dashboard_model "Continuous")

set(dashboard_cache "
BUILD_QtDialog:BOOL=TRUE
CMAKE_CXX_STANDARD:STRING=14
CMAKE_CXX_FLAGS:STRING=-Wall -W -Wunused-variable -Wunused-parameter -Wunused-function -Wunused -Wno-deprecated -Woverloaded-virtual
CMAKE_C_FLAGS:STRING=-Wall -W
CMAKE_Swift_COMPILER:FILEPATH=$ENV{SWIFTC}
CMake_RUN_CLANG_TIDY:BOOL=ON
CMake_RUN_IWYU:BOOL=OFF
CMake_TEST_FindGTK2:BOOL=ON
CMake_TEST_Qt5:BOOL=ON
CLANG_TIDY_COMMAND:FILEPATH=/usr/bin/clang-tidy-8
IWYU_COMMAND:FILEPATH=/home/kitware/Dashboards/Support/iwyu/bin/include-what-you-use
")

include(${CTEST_SCRIPT_DIRECTORY}/cmake/cmake_common.cmake)

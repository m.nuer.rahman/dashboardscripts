#
# OS: OpenSUSE 42.2 (aarch64)
# CPU: AMD Opteron-A SoC with 4 ARM Cortex-A57 cores
# GPU: None
# Compiler: (SUSE Linux) 6.2.1 20160826 [gcc-6-branch revision 239773] (aarch64)
# Maintainer: robert.maynard@kitware.com
#
cmake_minimum_required(VERSION 2.8)

set(ENV{CC} "/usr/bin/gcc-6")
set(ENV{CXX} "/usr/bin/g++-6")

set(maintainer_email_account "robert.maynard")
set(maintainer_email_domain "kitware.org")

set(CTEST_SITE "arrow.kitware")
set(CTEST_BUILD_NAME "OpenSUSE-Aarch64-GCC-6.2.1")
set(CTEST_BUILD_CONFIGURATION Release)
set(CTEST_CMAKE_GENERATOR "Unix Makefiles")

set(build_dir "CMake-gcc-6")
set(source_dir "${build_dir}-src")
set(CTEST_DASHBOARD_ROOT "$ENV{HOME}/Dashboards/MyTests")
set(CTEST_SOURCE_DIRECTORY "${CTEST_DASHBOARD_ROOT}/${source_dir}")
set(CTEST_BINARY_DIRECTORY "${CTEST_DASHBOARD_ROOT}/${build_dir}")

set(CTEST_GIT_COMMAND /usr/bin/git)

# Write initial cache.
set(dashboard_cache "
CMAKE_BUILD_TYPE:STRING=Release
BUILD_QtDialog:BOOL=OFF
CMake_TEST_FindOpenGL:BOOL=OFF
CMAKE_USE_OPENSSL:BOOL=OFF
")

include(${CTEST_SCRIPT_DIRECTORY}/cmake/cmake_common.cmake)

# Client maintainer: chuck.atkins@kitware.com
set(CTEST_SITE "aaargh.kitware.com")
set(CTEST_BUILD_NAME "Linux-EL7-Coverity")
set(CTEST_BUILD_CONFIGURATION Debug)
set(CTEST_CMAKE_GENERATOR "Ninja")
set(CTEST_BUILD_COMMAND "cov-build --dir cov-int ninja")
set(CTEST_USE_LAUNCHERS FALSE)
set(CTEST_DASHBOARD_ROOT "${CMAKE_CURRENT_BINARY_DIR}/Builds/Coverity")
find_program(CMAKE_MAKE_PROGRAM NAMES ninja PATHS /usr/local/bin)

set(dashboard_no_test TRUE)
set(dashboard_no_submit TRUE)
set(dashboard_no_KWSys TRUE)
set(ENV{PATH} "/opt/coverity/cov-analysis-linux64-2017.07/bin:$ENV{PATH}")

set(ENV{CC}  /usr/bin/gcc)
set(ENV{CXX} /usr/bin/g++)
set(ENV{FC}  /usr/bin/gfortran)

set(dashboard_cache "
CMAKE_USE_SYSTEM_LIBRARIES:BOOL=ON
CMAKE_USE_SYSTEM_LIBRARY_JSONCPP:BOOL=OFF
BUILD_TESTING:BOOL=OFF
")

include(${CTEST_SCRIPT_DIRECTORY}/cmake/cmake_common.cmake)

message("Collecting Coverity scan results")
execute_process(
  COMMAND ${CMAKE_COMMAND} -E tar cvfJ ${CTEST_BUILD_NAME}.tar.xz -- cov-int
  WORKING_DIRECTORY ${CTEST_BINARY_DIRECTORY}
)

message("Determining source version")
execute_process(
  COMMAND git describe
  WORKING_DIRECTORY ${CTEST_SOURCE_DIRECTORY}
  OUTPUT_VARIABLE cmake_version
)

message("Uploading Coverity scan results")
string(REGEX REPLACE "." "X" TOKEN_SAFE "$ENV{CMAKE_COVERITY_SUBMIT_TOKEN}")
message("DEBUG: curl --form token=${TOKEN_SAFE} --form email=chuck.atkins@kitware.com --form version=\"${cmake_version}\" --form description=\"Nightly\" --form file=@${CTEST_BUILD_NAME}.tar.xz \"https://scan.coverity.com/builds?project=CMake\"")

execute_process(
  COMMAND curl
    --form token=$ENV{CMAKE_COVERITY_SUBMIT_TOKEN}
    --form email=chuck.atkins@kitware.com
    --form version="${cmake_version}"
    --form description="Nightly"
    --form file=@${CTEST_BUILD_NAME}.tar.xz
    "https://scan.coverity.com/builds?project=CMake"
  WORKING_DIRECTORY ${CTEST_BINARY_DIRECTORY}
)

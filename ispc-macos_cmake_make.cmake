set(CTEST_SITE "ispc-macos.kitware")
set(CTEST_BUILD_NAME "macos-make")
set(CTEST_BUILD_CONFIGURATION Release)
set(CTEST_CMAKE_GENERATOR "Unix Makefiles")
set(CTEST_TEST_ARGS
  PARALLEL_LEVEL 14
  )

set(ENV{ISPC} "/Users/svc-dashboard/Dashboards/Support/ispc-v1.13.0/bin/ispc")

set(dashboard_model Nightly)
set(dashboard_source_name CMakeMake-src)
set(dashboard_binary_name CMakeMake-bld)

set(dashboard_cache "
CMake_TEST_ISPC:BOOL=ON
")

include(${CTEST_SCRIPT_DIRECTORY}/cmake/cmake_common.cmake)

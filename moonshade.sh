#!/usr/bin/env bash

export "PATH=${HOME}/Dashboards/bin:$PATH"

/Applications/CMake.app/Contents/bin/ctest -S ${HOME}/Dashboards/DashboardScripts/moonshade_cmake_ninja.cmake -V > ${HOME}/Dashboards/Logs/cmake_ninja.log 2>&1
/Applications/CMake.app/Contents/bin/ctest -S ${HOME}/Dashboards/DashboardScripts/moonshade_cmake_nmc.cmake -V > ${HOME}/Dashboards/Logs/cmake_nmc.log 2>&1
/Applications/CMake.app/Contents/bin/ctest -S ${HOME}/Dashboards/DashboardScripts/moonshade_cmake_xcode_ub.cmake -V -C Release > ${HOME}/Dashboards/Logs/cmake_xcode_ub.log 2>&1

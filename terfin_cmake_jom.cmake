set(CTEST_SITE "terfin.kitware")
set(CTEST_BUILD_NAME "vs15-32-jom")
set(CTEST_BUILD_CONFIGURATION Release)
set(CTEST_CMAKE_GENERATOR "NMake Makefiles JOM")

set(exclude
  ExternalProject # Fails intermittently.  Needs investigation.
  )
string(REPLACE ";" "|" exclude "${exclude}")
set(CTEST_TEST_ARGS
  PARALLEL_LEVEL 16
  EXCLUDE "^(${exclude})$"
  )

macro(dashboard_hook_init)
  set(JOM_DIR "${CTEST_DASHBOARD_ROOT}/jom")
  file(REMOVE_RECURSE "${JOM_DIR}")
  file(MAKE_DIRECTORY "${JOM_DIR}")
  set(JOM_URL "https://download.qt-project.org/official_releases/jom/unstable-jom.zip")
  file(DOWNLOAD "${JOM_URL}" "${JOM_DIR}/jom.zip" STATUS stat LOG log)
  list(GET stat 0 res)
  if(NOT res EQUAL 0)
    message(FATAL_ERROR "Error in download ${JOM_URL}: ${stat} ${log}")
  endif()
  execute_process(
    COMMAND ${CMAKE_COMMAND} -E tar xf jom.zip
    WORKING_DIRECTORY ${JOM_DIR}
    RESULT_VARIABLE res
    OUTPUT_VARIABLE log
    ERROR_VARIABLE log
    )
  if(NOT res EQUAL 0)
    message(FATAL_ERROR "Error in unzip: ${res}\n${log}")
  endif()

  set(ENV{PATH} "${JOM_DIR};$ENV{PATH}")
endmacro()

set(dashboard_source_name CMake-${CTEST_BUILD_NAME}-src)
set(dashboard_binary_name CMake-${CTEST_BUILD_NAME}-bld)

set(dashboard_cache "
CMake_TEST_EXTERNAL_CMAKE:PATH=C:/Dashboards/My Tests/CMake-vs15-64-ninja-bld/bin
CMake_TEST_IPO_WORKS_C:BOOL=ON
CMake_TEST_IPO_WORKS_CXX:BOOL=ON
")

set(dashboard_no_KWSys 1)
include(${CTEST_SCRIPT_DIRECTORY}/cmake/cmake_common.cmake)
